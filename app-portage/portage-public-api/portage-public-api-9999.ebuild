# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit git-2 eutils

DESCRIPTION="This is the public-api to portage under development for consumer app use."
HOMEPAGE="http://git.overlays.gentoo.org/gitweb/?p=proj/portage.git;a=shortlog;h=refs/heads/public_api"
EGIT_REPO_URI="git://git.overlays.gentoo.org/proj/portage.git"
EGIT_BRANCH="public_api"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	>=sys-apps/portage-2.1.9.42
	!=sys-apps/portage-8888
"
RDEPEND="${DEPEND}"

src_install() {
	API_PATH="pym/portage/api/"
	dodir /usr/$(get_libdir)/portage/${API_PATH}
	insinto /usr/$(get_libdir)/portage/${API_PATH}
	for file in `ls ${S}/${API_PATH}`; do
		newins  ${S}/${API_PATH}/$file $file
	done
}
